#include <Arduino.h>
#include <LoRaCom.h>

#define LORA_NSS  15
#define LORA_DIO0  5
#define LORA_RESET 4
#define LORA_MOSI 13
#define LORA_MISO 12
#define LORA_SCLK 14

LoRaCom * LoRaCMD;

char str [200]="0123456789012345";

void setup() {
  delay(700);
  LoRa.setPins(LORA_NSS, LORA_RESET, LORA_DIO0);
  Serial.begin(4032000);
  LoRaCMD = new LoRaCom(&LoRa);
  Serial.printf("IsReady? %d\n", LoRaCMD->isReady());
  LoRaCMD->rxStart();
}

void loop() {
  LoRaCMD->keep();
  if(LoRaCMD->RxAvail()) LoRaCMD->popLastRxCont();
}