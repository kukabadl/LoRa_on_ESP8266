#include <LoRa.h>
#include <queue>
#include <vector>
enum class LoRaMode {STANDBY = 0, RX = 1, TX = 2};
enum class Cmd {STANDBY = 0, RX = 1, TX = 2};
LoRaMode LoRaM = LoRaMode::STANDBY;

typedef struct LoRaRxSt{
    uint8_t senderAddr = 0;
    uint8_t targetAddr = 0;
    int16_t status = 1;
    uint8_t data [255];
    uint8_t len = 0;
    int rssi = 0;
    float snr = 0;
    long fErr = 0;
    void print(){
        Serial.printf("\tFrom: %d To: %d\n", senderAddr, targetAddr);
        // print RSSI (Received Signal Strength Indicator)
        Serial.printf("\tRSSI:\t\t%d dBm.\n", rssi);

        // print SNR (Signal-to-Noise Ratio)
        Serial.print("\tSNR:\t\t");
        Serial.print(snr); Serial.println(" dB.");

        // print frequency error
        Serial.printf("\tFreq error:\t%ld Hz\n", fErr);
        Serial.print("\tData:\t\t");
        for(int i = 0; i < len; i++){
            Serial.print((char)data[i]);
        }
        Serial.println(); Serial.println();
    }
} LoRaRxCont;

typedef struct LoRaTxSt{
    uint8_t senderAddr = 0;
    uint8_t targetAddr = 255;
    uint8_t len = 0;
    uint8_t data[255];
    void print(){
        Serial.printf("\tFrom: %d To: %d\n", senderAddr, targetAddr);
        Serial.print("\tData: ");
        for(int i = 0; i < len; i++){
            Serial.print((char)data[i]);
        }
        Serial.println(); Serial.println();
    }
} LoRaTxCont;



//LoRaMode indicates which mode the module is in. 
//It is either 0(STANDBY), 1(RX) or 2(TX)

volatile bool enISR = false;
volatile bool DoneFlag = false;

class LoRaCom {
private:
    bool ready;
    LoRaClass * pLoRa;
    void LoRaInit(LoRaClass * pLora);
    std::queue<LoRaRxCont>RxFiFo;
    std::queue<LoRaTxCont>TxFiFo;
    std::vector<uint8_t>usedAddr;

    int packageLen;
    uint32_t transaction_time;
    
    #ifndef ESP8266
        SPIClass * pLoRaSPI;
    #endif

public:
    LoRaCom();
    void keep();
    LoRaCom(LoRaClass * pLora);
    void setPlen(int len){packageLen = len;};
    LoRaRxCont getLastRxCont(){return RxFiFo.front();};
    LoRaRxCont popLastRxCont();
    void done(){transaction_time = millis() - transaction_time;};
    bool isReady(){return ready;};
    bool RxAvail();
    void rxRead();
    uint32_t tTime(){return transaction_time;};
    void rxStart();
    void Tx(String data);
    void Tx(LoRaTxCont toTx);
    void Tx(uint8_t len, uint8_t * data);
    void startTx(LoRaTxCont toTx);
    #ifndef ESP8266
        LoRaCom(LoRaClass * pLora, SPIClass * pSPI);
        void setSPIPins(uint8_t MISO, uint8_t MOSI, uint8_t SCK);
    #endif
};

extern LoRaCom * LoRaCMD;

#ifdef ESP8266
    ICACHE_RAM_ATTR void TxDone() {
        // check if the interrupt is enabled
        if(enISR) {
            LoRaCMD->done();
            Serial.printf("Tx finished in %d ms.\n", LoRaCMD->tTime());
            DoneFlag = true;
        }
        return;
        // we got a packet, set the flag
    }

    ICACHE_RAM_ATTR void RxDone(int size) {
        // check if the interrupt is enabled
        if(enISR) {
            LoRaCMD->done();
            LoRaCMD->setPlen(size);
            Serial.printf("Rx finished in %d ms.\n", LoRaCMD->tTime());
            DoneFlag = true;
        }
        return;
        // we got a packet, set the flag
    }
#else
    void TxDone() {
        // check if the interrupt is enabled
        if(enISR) {
            LoRaCMD->done();
            Serial.printf("Tx finished in %d ms.\n", LoRaCMD->tTime());
            DoneFlag = true;
        }
        return;
        // we sent a packet, set the flag
    }

    void RxDone(int size) {
        // check if the interrupt is enabled
        if(enISR) {
            LoRaCMD->done();
            LoRaCMD->setPlen(size);
            Serial.printf("Rx finished in %d ms.\n", LoRaCMD->tTime());
            DoneFlag = true;
        }
        return;
        // we got a packet, set the flag
    }

    void LoRaCom::setSPIPins(uint8_t MISO, uint8_t MOSI, uint8_t SCK){
        pLoRaSPI->setMOSI(MOSI);
        pLoRaSPI->setMISO(MISO);
        pLoRaSPI->setSCLK(SCK);
    }

    LoRaCom::LoRaCom(LoRaClass * pLora, SPIClass * pSPI){
        pLoRaSPI = pSPI;
        LoRaInit(pLora);
    }
#endif


LoRaCom::LoRaCom(){
    LoRaInit(NULL);
}

LoRaCom::LoRaCom(LoRaClass * pLora){
    LoRaInit(pLora);
}
bool LoRaCom::RxAvail(){
    return !RxFiFo.empty();
}
LoRaRxCont LoRaCom::popLastRxCont(){
    
        LoRaRxCont temp = RxFiFo.front();
        RxFiFo.pop();
        return temp;
}

void LoRaCom::LoRaInit(LoRaClass * pLora){
    usedAddr.reserve(30);
    if (pLora != NULL) {
        pLoRa = pLora;
        Serial.println(); Serial.println();
        Serial.print(F("Initialization of LoRa module... "));
        for (int i = 0; !LoRa.begin(433E6); i++) {             //Initialize radio at 433 MHz
            if(i > 5){
                Serial.printf("Initialization failed %d times. Aborting.\n", i);
                ready = false;
                return;
            }
            Serial.printf("Attemp #%d failed.\n", i);
            delay(300);
        }
        Serial.println("Successful!");
        pLoRa->onReceive(RxDone);
        pLoRa->onTxDone(TxDone);
        
        pLoRa->setPreambleLength(6);
        pLoRa->setSignalBandwidth(500000);
        pLoRa->setSpreadingFactor(7);
        ready = true;
    }
    else ready = false;
    return;
}

void LoRaCom::keep(){
    if (DoneFlag){
        DoneFlag = false;
        if(LoRaM == LoRaMode::RX) rxRead();        
        if(LoRaM == LoRaMode::TX){
            TxFiFo.pop();
            if (!TxFiFo.empty()){
                startTx(TxFiFo.front());
            }
            else rxStart();
        }
    }
    else if(LoRaM == LoRaMode::RX && !TxFiFo.empty()){
        startTx(TxFiFo.front());
    }

}


void LoRaCom::rxStart(){
    enISR = true;
    LoRaM = LoRaMode::RX;
    transaction_time = millis();
    pLoRa->receive();
}

void LoRaCom::Tx(LoRaTxCont toTx){TxFiFo.push(toTx);}

void LoRaCom::Tx(uint8_t len, uint8_t * data){
    LoRaTxCont temp;
    for(int i = 0; i < len && i <= 255; i++){
        temp.data[i] = data[i];
    }
    temp.len = len;
    TxFiFo.push(temp);
}

void LoRaCom::Tx(String data){
    LoRaTxCont temp;
    for(uint16_t i = 0; i < data.length() && i < 256; i++){
        temp.data[i] = data[i];
    }
    temp.len = data.length();

    TxFiFo.push(temp);
}

void LoRaCom::startTx(LoRaTxCont toTx){
    enISR = true;
    LoRaM = LoRaMode::TX;
    pLoRa->beginPacket();
    pLoRa->write(toTx.targetAddr); pLoRa->write(toTx.senderAddr);
    pLoRa->write((const uint8_t *) toTx.data, toTx.len);
    pLoRa->endPacket(true);
    Serial.printf("Sending:\n");
    toTx.print();
    transaction_time = millis();
}

void LoRaCom::rxRead(){
    enISR = false;
    LoRaRxCont recv;
    recv.snr = pLoRa->packetSnr();
    recv.rssi = pLoRa->packetRssi();
    packageLen -= 2;
    if(recv.snr > 2 && packageLen <= 0xff && packageLen > 0){
        recv.fErr = pLoRa->packetFrequencyError();
        recv.len = packageLen;
        recv.targetAddr = pLoRa->read();
        recv.senderAddr = pLoRa->read();
        for(uint8_t i = 0; i < recv.len; i++)recv.data[i] = pLoRa->read();
        RxFiFo.push(recv);
        recv.print();
    }
    else {
        Serial.printf("Bad package, ignoring. (SNR = %f; len = %d)\n", recv.snr, packageLen);
    }
    

    // put module back to listen mode
    pLoRa->receive();
    transaction_time = millis();
    enISR = true;
    return;
}